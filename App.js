import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import SignUp from "./src/screen/signup/signup";
import SignIn from './src/screen/signin/signin';
import ValidateLogin from './src/screen/validate-login/validate-login';
import Chat from './src/screen/chat/chat';
import CameraScreen from './src/screen/camera-screen/camera-screen';

var authStack = createStackNavigator({
  Signup: {
    screen: SignUp
  },
  Signin: {
    screen: SignIn
  },
  ValidateLogin: {
    screen: ValidateLogin
  },
  Camera: {
    screen: CameraScreen
  }
}, {
  initialRouteName: "ValidateLogin"
});

var chatStack = createStackNavigator({
  Chat: {
    screen: Chat
  }
});

export default createSwitchNavigator({
  AuthStack: authStack,
  ChatStack: chatStack
});