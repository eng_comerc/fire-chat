import React from 'react';
import { StyleSheet, Text, View, Button, TextInput, ActivityIndicator } from 'react-native';
import firebaseService from "../../services/firebase.service";

export default class Loading extends React.Component {
  render() {
    return (
      <View style={{
        flex: 1,
      }}>
        { this.props.loading ? 
        <View style={styles.container}>
          <ActivityIndicator size="large" color="#0000ff" /> : 
        </View>
        :
        <View style={[{
          flex: 1,
        },this.props.styles]}>
          {this.props.children}
        </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {width: 260, height: 30, borderColor: 'gray', borderWidth: 1, marginTop: 10}
});
