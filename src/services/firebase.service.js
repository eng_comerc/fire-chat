import * as firebase from 'firebase';
import { AsyncStorage } from "react-native"

const USER_STORAGE_NAME = "USER_STORAGE";

const firebaseConfig = {
    apiKey: "AIzaSyCpEa60qprSPNxVG6G3Uen5xbuCJaW8spo",
    authDomain: "fire-chat-73ef2.firebaseapp.com",
    databaseURL: "https://fire-chat-73ef2.firebaseio.com",
    projectId: "fire-chat-73ef2",
    storageBucket: "fire-chat-73ef2.appspot.com",
    messagingSenderId: "831047519604"
};

class FirebaseService {
    constructor() {
        firebase.initializeApp(firebaseConfig);
        console.log("Firebase Inited");
    }

    sendMessage(message) {
        return new Promise((resolve, reject) => {
            firebase.database().ref("messages").push(message, error => {
                if(error){
                    reject(error);
                }
                resolve(true);
            })
        })
    }

    getMessages() {
        return new Promise((resolve, reject) => {
            firebase.database().ref("messages").on("value", snapshot => {
                var messages = [];
                let count = 0;
                snapshot.forEach(child => {
                    messages.push({
                        _id: count,
                        ...child.val()
                    });
                    count++;
                })
                resolve(messages);
            })
        })
    }

    getUser() {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(USER_STORAGE_NAME).then(user => {
                resolve(JSON.parse(user));
            })
        })
    }

    createUser(email, password, name, nickname){
        return new Promise((resolve, reject) => {
            firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(email, password)
                .then(({user}) => {
                    var usr = {
                        email: email,
                        name: name,
                        nickname,
                        uid: user.uid,
                        created: new Date()
                    }
                    firebase.database().ref("users").child(user.uid).set(usr, error => {
                        if(error){
                            reject(error);
                        }
                        AsyncStorage.setItem(USER_STORAGE_NAME, JSON.stringify(usr)).then(() => {
                            resolve(usr);
                        })
                    })
                }).catch(error => {
                    reject(error);
                })
        })
    }

    login(email, senha) {
        return new Promise((resolve, reject) => {
            firebase.auth().signInWithEmailAndPassword(email, senha)
                .then(({user}) => {
                    firebase.database().ref("users/" + user.uid).on("value", snapshot => {
                        var usr = snapshot.val();
                        AsyncStorage.setItem(USER_STORAGE_NAME, JSON.stringify(usr)).then(() => {
                            resolve(user);
                        });
                    })
                }).catch(error => {
                    reject(error);
                })
        })
    }

    isLoggedIn(){
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(USER_STORAGE_NAME).then(user => {
                resolve(user != null && user != undefined);
            })
        })
    }

    logout() {
        return new Promise((resolve, reject) => {
            AsyncStorage.removeItem(USER_STORAGE_NAME).then(() => {
                resolve(true);
            })
        }) 
    }
}

var firebaseService = new FirebaseService();
export default firebaseService;