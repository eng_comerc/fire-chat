import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import firebaseService from "../../services/firebase.service";
import Loading from '../../shared/loading/loading';

export default class ValidateLogin extends React.Component {
  static navigationOptions = {
    header: null
  };

  state = {
    loading: true
  }

  componentWillMount(){
    firebaseService.isLoggedIn().then(logged => {
      if(logged){
        this.props.navigation.navigate("Chat");
      }else{
        this.props.navigation.navigate("Signin");
      }
    })
  }

  goToPage() {
    
  }

  render() {
    return (
      <View style={styles.container}>
          <Loading loading={true} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {width: 260, height: 30, borderColor: 'gray', borderWidth: 1, marginTop: 10}
});
