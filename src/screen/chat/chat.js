import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import firebaseService from "../../services/firebase.service";
import { GiftedChat } from 'react-native-gifted-chat'
import Loading from "../../shared/loading/loading";

export default class Chat extends React.Component {
  static navigationOptions = ({navigation}) => {
    return {
      title: navigation.state.params ? navigation.state.params.title : "",
      headerRight: navigation.state.params ? navigation.state.params.rightButton : null
    };
  }

  state = {
    messages: [],
    user: null,
    loading: true,
    message: ""
  }

  logout() {
    firebaseService.logout();
    this.props.navigation.navigate("Signin");
  }

  componentDidMount() {
    this.props.navigation.setParams({
      title: "Chat Page",
      rightButton: (
        <Button  
          title="Logout"
          onPress={() => {
            this.logout();
          }}
          />
      )
    })
    firebaseService.getUser().then(user => {
      this.setState({
        user: user,
        loading: false
      }, () => {
        this.loadMessages();
      });
    })
  }

  loadMessages(){
    firebaseService.getMessages().then(messages => {
      console.log(messages)
      this.setState({
        messages
      })
    })
  }

  onSend() {
    var message = {
      text: this.state.message,
      createdAt: (new Date()).toISOString(),
      user: {
        _id: this.state.user.uid,
        name: this.state.user.name,
        avatar: 'https://placeimg.com/140/140/any',
      }
    }
    firebaseService.sendMessage(message).then(() => {
      this.loadMessages()
    })
  }

  render() {
    return (
      <View style={styles.container}>
          <Loading loading={this.state.loading}>
            <GiftedChat
              messages={this.state.messages}
              onSend={messages => this.onSend()}
              onInputTextChanged={(message) => this.setState({message})}
              user={{
                _id: 1,
              }}
            />
          </Loading>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff'
  },
  input: {width: 260, height: 30, borderColor: 'gray', borderWidth: 1, marginTop: 10}
});
