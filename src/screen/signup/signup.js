import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import firebaseService from "../../services/firebase.service";
import Loading from '../../shared/loading/loading';
import { Container, Header, Content, Accordion } from "native-base";
const dataArray = [
  { title: "First Element", content: "Lorem ipsum dolor sit amet" },
  { title: "Second Element", content: "Lorem ipsum dolor sit amet" },
  { title: "Third Element", content: "Lorem ipsum dolor sit amet" }
];


export default class SignUp extends React.Component {
  state = {
    email: "",
    password: "",
    name: "",
    nickname: "",
    loading: false
  }

  changeLoadingState(state, callback) {
    this.setState({
      loading: state
    }, () => {
      if(callback){
        callback();
      }
    })
  }

  createUser() {
    this.changeLoadingState(true);
    firebaseService.createUser(
      this.state.email,
      this.state.password,
      this.state.name,
      this.state.nickname
    ).then(user => {
      this.changeLoadingState(false, () => {
        this.props.navigation.navigate("Chat");
      });
    }).catch(err => {

    })
  }

  render() {
    return (
      <Loading loading={this.state.loading}>
        <View style={styles.container}>
          <TextInput
            style={styles.input}
            placeholder="Nome"
            onChangeText={(name) => this.setState({ name })}
          />
          <TextInput
            style={styles.input}
            placeholder="Nickname"
            autoCapitalize="none"
            onChangeText={(nickname) => this.setState({ nickname })}
          />
          <TextInput
            style={styles.input}
            placeholder="E-mail"
            keyboardType="email-address"
            autoCapitalize="none"
            onChangeText={(email) => this.setState({ email })}
          />
          <TextInput
            style={styles.input}
            placeholder="Senha"
            secureTextEntry={true}
            onChangeText={(password) => this.setState({ password })}
          />
          <Button
            title="Enviar"
            onPress={() => {
              this.createUser();
            }}
          />
        </View>
      </Loading>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: { width: 260, height: 30, borderColor: 'gray', borderWidth: 1, marginTop: 10 }
});
