import React from 'react';
import { StyleSheet, Text, View, Button, TextInput } from 'react-native';
import firebaseService from "../../services/firebase.service";
import Loading from '../../shared/loading/loading';

export default class SignIn extends React.Component {
  static navigationOptions = {
    title: 'Home',
    header: null
  };

  state = {
    email: "",
    password: "",
    loading: false
  }

  changeLoadingState(state) {
    this.setState({
      loading: state
    })
  }

  login() {
    this.changeLoadingState(true);
    firebaseService.login(this.state.email, this.state.password).then(user => {
      this.changeLoadingState(false);
      this.props.navigation.navigate("ChatStack");
    })
  }

  render() {
    return (
      <Loading 
        loading={this.state.loading}
        style={{
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View style={styles.container}>
          <TextInput
            style={styles.input}
            placeholder="E-mail"
            keyboardType="email-address"
            autoCapitalize="none"
            onChangeText={(email) => this.setState({ email })}
          />
          <TextInput
            style={styles.input}
            placeholder="Senha"
            secureTextEntry={true}
            onChangeText={(password) => this.setState({ password })}
          />
          <Button
            title="Enviar"
            onPress={() => {
              this.login();
            }}
          />

          <Button
            title="Cadastre-se"
            onPress={() => {
              this.props.navigation.navigate("Signup");
            }}
          />
        </View>
      </Loading>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: { width: 260, height: 30, borderColor: 'gray', borderWidth: 1, marginTop: 10 }
});
